const llamandoFetch = ()=>{
    const http = new XMLHttpRequest;
    const currId = document.querySelector('#input').value;
    const url = `https://jsonplaceholder.typicode.com/albums/${currId}`;
    
    http.open('GET', url, true);
	http.send();
    
    if (currId > 0) {
        http.onreadystatechange = function () {
            if (this.status == 200 && this.readyState == 4) {
                const json = JSON.parse(this.responseText);
                document.querySelector('.resultado').innerHTML = json.title;
            }
            else if (this.readyState == 4) {
                alert("Error");
            }
        }
    }
    else {
        alert("Ingrese otro numero");
    }

}

document.querySelector("#btnConsultar").addEventListener("click", () => {
    llamandoFetch();
});

function hacerPeticion() {
	const http = new XMLHttpRequest;
	const currId = document.querySelector('#input').value;
	const url = `https://jsonplaceholder.typicode.com/users/${currId}`;
	
	http.open('GET', url, true);
	http.send();

	if (currId > 0) {
		http.onreadystatechange = function () {
			if (this.status == 200 && this.readyState == 4) {
				// Aquí se dibuja la página
				const json = JSON.parse(this.responseText);

				// CICLO PARA IR TOMANDO CADA UNO DE LOS REGISTROS
				document.querySelector('.resultado')
				.innerHTML =
				`
				<b>ID:</b> ${json.id}</br>
				<b>Nombre:</b> ${json.name}</br>
				<b>Usuario:</b> ${json.username}</br>
				<b>Email:</b> ${json.email}</br>
				<b>Teléfono:</b> ${json.phone}</br>
				<b>Website:</b> ${json.website}</br>
				<b>Direccion:</b> </br>
				${json.address.street} </br>
				${json.address.suite}</br>
				${json.address.city}</br>
				${json.address.zipcode}</br>
				${json.address.geo.lat}</br>
				${json.address.geo.lng}</br>
				<b>Compañia:</b> </br>
				${json.company.name} </br>
				${json.company.catchPhrase}</br>
				${json.company.bs}</br>
				`
			}
			else if (this.readyState == 4) {
				alert("Error");
			}
		}
	}
	else {
		alert("Ingrese otro numero");
	}
}

document.querySelector(".btnConsultar").addEventListener("click", () => {
	hacerPeticion();
});

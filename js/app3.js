function cargarDatos(){
    const http = new XMLHttpRequest;
    const url = "https://jsonplaceholder.typicode.com/users";

    http.onreadystatechange = function(){
        
        if(this.status == 200 && this.readyState==4){

            let res = document.getElementById('Lista');
            const json = JSON.parse(this.responseText);

            for(const datos of json){   

                const address = datos.address;
                const geo = address.geo;
                const company = datos.company;

                res.innerHTML += '<tr> <td class="columna1">' + datos.id + '</td>'
                + '<td class=columna2">' + datos.name + '</td>'
                + '<td class=columna3">' + datos.username + '</td>'
                + '<td class=columna4">' + datos.email + '</td>'
                + '<td class=columna5"> <ul> <li> ' + address.street + '</li>' 
                + '<li>' + address.suite + '</li>' +  '<li>' + address.city + '</li>'
                + '<li>' + address.zipcode + '</li>'
                + '<li> geo: <ol> <li>' + geo.lat + '</li>' + '<li>' + geo.lng + '</li>'
                + '<ol> </li>' + '</li> </td>'
                + '<td class=columna6">' + datos.phone + '</td>'
                + '<td class=columna7">' + datos.website + '</td>'
                + '<td class=columna8"> <ul> <li>' + company.name + '</li>'
                + '<li>' + company.catchPhrase + '</li>' + '<li>' + company.bs + '</li> </tr>';

            }

        } 

    }

    http.open('GET',url,true);
    http.send();

}

document.getElementById("btnCargar").addEventListener("click",cargarDatos);
document.getElementById("btnLimpiar").addEventListener("click",function(){

    let res = document.getElementById('Lista');
    res.innerHTML="";

})
